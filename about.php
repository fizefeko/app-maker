<?php include "views/header.php" ?>

<div id="About">
    <div class="Container">
        <div class="top-text">
            <h2>-Our Mission-</h2>
            <span>To provide the most actionable app store data.</span>
            <h2>About</h2>
            <p>Lorem Ipsum is simply dummy text of the printing 
                and typesetting industry. Lorem Ipsum has been the industry's
                 standard dummy text ever since the 1500s, when an unknown printer
                  took a galley of type and scrambled it to make a type specimen book.
                   It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of 
                    Letraset sheets containing Lorem Ipsum passages, and more recently with desktop 
                publishing software like Aldus
                 PageMaker including versions of Lorem Ipsum</p>
        </div>
        <div class="our-team">
            <div class="person">
                <img src="img/workers/photo1.png" alt="">
                <span class="name">Fitim zenuni</span>
                <span class="title">CEO</span>
                <p>Eli drives Apptopia’s strategic vision and manages investor relations.
                         Before Apptopia Eli was involved in several startups,
                     including: GPush, Oasys Water, GreatPoint Energy, and DVTel.</p>
            </div>
            <div class="person">
                <img src="img/workers/photo2.png" alt="">
                <span class="name">Defrim Zenuni</span>
                <span class="title">Programer</span>
                <p>Eli drives Apptopia’s strategic vision and manages investor relations.
                         Before Apptopia Eli was involved in several startups,
                     including: GPush, Oasys Water, GreatPoint Energy, and DVTel.</p>
            </div>
            <div class="person">
                <img src="img/workers/photo3.png" alt="">
                <span class="name">Dhurim Zenuni</span>
                <span class="title">DESIGNER</span>
                <p>Eli drives Apptopia’s strategic vision and manages investor relations.
                         Before Apptopia Eli was involved in several startups,
                     including: GPush, Oasys Water, GreatPoint Energy, and DVTel.</p>
            </div>
            <div class="person">
                <img src="img/workers/photo4.png" alt="">
                <span class="name">Hana Montana</span>
                <span class="title">Marketing Manager</span>
                <p>Eli drives Apptopia’s strategic vision and manages investor relations.
                         Before Apptopia Eli was involved in several startups,
                     including: GPush, Oasys Water, GreatPoint Energy, and DVTel.</p>
            </div>
        </div>
    </div>
</div>

<?php include "views/footer.php" ?>