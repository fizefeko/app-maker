<?php
//Create 
    $name_error = $email_error = $message_error = "";
    $name = $email = $message = $succes = "";
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(empty($_POST["name"])){
            $name_error = "Name is required";
        }else{
                $name = test_input($_POST["name"]);
                if(!preg_match("/^[a-zA-Z ]*$/",$name)){
                    $name_error = "Only letters and whitespaces are allowed";
                }
        }
        
        
        if(empty($_POST["email"])){
                $email_error = "E-mail is required";
        }else{
            $email = test_input($_POST["email"]);
            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $email_error = "invalid e-mail format";
            }
        }
        
        
        if(empty($_POST["message"]) or strlen($_POST["message"])<30){
            $message_error = "You can't send an empty message or a string that is shorter than 11 characters";
        }else{
            $message = test_input($_POST["message"]);
        }  
        
        
        if($name_error == "" and $email_error == "" and $message_error == ""){
            $message_body = "";
            unset($_POST['submit']);
            foreach($_POST as $key => $value){
                $message_body .= "$key: $value\n";
            }
            $recivier = 'fizefeko@gmail.com';
            $subject = 'AppMaker e-mail';
            if(mail($recivier,$subject,$message_body)){
                $name = $email = $message = "";
                $succes = "Message sent ,thank you for contacting us";

            }
        }           
    }
        


    function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>