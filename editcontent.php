<?php 
    include "views/header.php"; 
    if(isset($_SESSION['IsAdmin'])){
        if($_SESSION['IsAdmin'] == '1'){
            include "includes/editcontent.inc.php";
        }else{
            die('Only admins are allowed to edit content!');
        }
    }else{
        die('You are not loged in!');      
    }

 ?>

    <div style="padding: 70px 0;" id="EditFeatures">
        <div class="Container">
            <form  action="<?php $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
               
                <fieldset>
                    <input type="text" placeholder="Subtitle" name="subtitle" value="<?php echo $subtitle;?>">
                    <span class="ContactFormError"><?php echo $subtitle_error; ?></span>
                </fieldset>

                <fieldset>
                    <textarea id="" cols="30" rows="10" placeholder="Text of the content" name="content" ><?php echo $content;?></textarea>
                    <span class="ContactFormError"><?php echo $content_error; ?></span>
                </fieldset>       
               
                <fieldset>
                    <input id="FileUploader" type="file" name="image"> </fieldset> 
                    <span class="ContactFormError"><?php echo $extension_error; ?></span>
                    <span class="ContactFormError"><?php echo $uploading_error; ?></span>
                </fieldset> 
               
                <fieldset>
                    <button id="Buttonn" type="submit">Add Content</button>
                    <span class="Success"><?php echo $success; ?></span>
                </fieldset>
            </form>
        </div>
    </div>

<?php include "views/footer.php"; ?>