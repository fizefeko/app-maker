<?php 
    include 'views/header.php';
    include 'includes/signup.inc.php';
 ?>  
    
    <form id="InputForm" action="<?php $_SERVER['PHP_SELF'];?>" method="post">
        <div class="Container">
            <fieldset>
                <input type="text" name="firstname" placeholder="Name" value="<?php echo $firstname;?>"><span class="ContactFormError"><?php echo $firstname_error; ?></span>
            </fieldset>
            
            <fieldset>
                <input type="text" name="lastname" placeholder="Last name"  value="<?php echo $lastname;?>" ><span class="ContactFormError"><?php echo $lastname_error; ?></span>
            </fieldset>
            
            <fieldset>
                <input type="email" name="email" placeholder="E-mail" value="<?php echo $email;?>"><span class="ContactFormError"><?php echo $email_error; ?></span>
            </fieldset>
            
            <fieldset>
                <input type="text" name="username" placeholder="Username" value="<?php echo $username;?>"><span class="ContactFormError"><?php echo $username_error; ?></span>
            </fieldset>
            
            <fieldset>
                <label>Male</label><input type="radio" name="gender" value="male" checked="checked">
                <label>Female</label><input type="radio" name="gender" value="female"><span class="ContactFormError"><?php echo $gender_error; ?></span>
            </fieldset>
            <fieldset>
                <input onfocus="(this.type='date')" onblur="(this.type='text')" name="birthdate" placeholder="Birth date" value="<?php echo $birthdate;?>"><span class="ContactFormError"><?php echo $birthdate_error; ?></span>
            </fieldset>
            
            <fieldset>
                <input type="password" name="password" placeholder="Password"><span class="ContactFormError"><?php echo $password_error; ?></span>
            </fieldset>
            <span class="Success"><?php echo $success; ?></span>
            <button name="signup">Register</button>
       </div>
    </form>
    
<?php include 'views/footer.php'; ?>